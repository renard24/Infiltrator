from cocos.sprite import Sprite
from cocos.euclid import Vector2
import cocos.collision_model as cm
from cocos.rect import Rect
from perso import *
from game_over import *
import os
import math
from cocos.director import director

SPEED_G=150


class Garde (Sprite):
    def __init__(self,image, image_detection,personnage, path, layer):
        super (Garde, self).__init__(image)
        self.personnage=personnage
        self.niv=self.personnage.level.actuel

        paths=[]
        texte=""
        #On ouvre le fichier
        with open(path,'r') as fichier:
            for line in fichier:
                texte += line

        #On parcourt les lignes du fichier
        paths = texte.split("\n")
        for k in range(0,len(paths),1):
            paths[k] = paths[k].split(",")
            for j in range(0,len(paths[k]),1):
                paths[k][j] = paths[k][j].split()
        for k in range(0,len(paths)-1,1):
            for j in range(0,len(paths[k]),1):
                paths[k][j][0] = int(paths[k][j][0])
                paths[k][j][1] = int(paths[k][j][1])
        #On sauvegarde cette structure

        self.path=paths[self.niv-1]
        self.x=self.path[0][0]
        self.y=self.path[0][1]
        self.kpath=0
        self.detection=Sprite(image_detection)
        self.detection.scale_y=2
        self.detection.anchor=(-self.detection.width/2,0)
        self.detection.position=(self.x+self.width/2,self.y)
        self.schedule_interval(self.move,0.01)
        layer.add(self.detection)

    def move(self,dt):
        i=self.path[self.kpath]
        x=i[0]-self.x
        y=i[1]-self.y
        self.Pos=Vector2(x,y)
        self.Pos.normalize()

        deg=math.atan2(-y,x)*180/math.pi
        self.rotation= deg

        n_x=self.Pos[0]*dt*SPEED_G
        n_y=self.Pos[1]*dt*SPEED_G

        #self.do(MoveBy((n_x,n_y),dt))
        self.x =self.x + n_x
        self.y = self.y+n_y

        x=i[0]-self.x
        y=i[1]-self.y

        if x>=-5 and x<=5 and y<=5 and y>=-5:
            self.kpath=(self.kpath+1)%len(self.path)
        self.vision()



    def vision(self):
        self.detection.x=self.x+self.detection.width/2
        self.detection.y=self.y
        self.detection.rotation=self.rotation
        p_y=self.personnage.sprite.position[1]+self.personnage.y
        p_x=self.personnage.sprite.position[0]+self.personnage.x

        deg=math.atan2(-(self.y-p_y),self.x-p_x)*180/math.pi
        rotf= (self.rotation-deg-180)%360

        if math.sqrt((p_x-self.x)*(p_x-self.x)+(p_y-self.y)*(p_y-self.y))<self.detection.width and (rotf<10 or rotf>350):
            director.replace(Scene(Game_over("fonds/game_over.png")))

        #self.detection.cshape=(cm.AARectShape(Vector2(self.detection.x,self.detection.y),self.detection.width/2,self.detection.height/2))

        #self.collision_manager = cm.CollisionManagerBruteForce()
        #self.collision_manager.clear()

        #self.collision_manager.add(self.detection.cshape)
        #self.collision_manager.add(self.personnage.sprite.cshape)


        #print(self.collision_manager.they_collide(self.detection,self.personnage.sprite))
        #if detection_rect.intersect(perso_rect)!= None :
            #print("game over !")
