from cocos.layer import Layer
from cocos.sprite import Sprite
from cocos.director import director
from pyglet.window import key
from random import randint
from level import *
import time

class Jeu_hack(Layer):
    is_event_handler=True
    def __init__(self,image_fond,cadre_select,n,level):
        win_w,win_h=director.get_window_size()
        super(Jeu_hack,self).__init__()
        self.level=level
        self.select=Sprite(cadre_select)
        self.back_ground=Sprite(image_fond)
        self.back_ground.scale_x=win_w/self.back_ground.width
        self.back_ground.scale_y=win_h/self.back_ground.height
        self.back_ground.x=self.back_ground.width/2
        self.back_ground.y=self.back_ground.height/2
        self.select.scale_x=self.back_ground.scale_x
        self.select.scale_y=self.back_ground.scale_y
        self.select.x=self.back_ground.width/2
        self.select.y=self.back_ground.height/2

        self.case_courante=0
        self.list_cases=["mini_jeu/virgo.png","mini_jeu/aquarius.png","mini_jeu/aries.png","mini_jeu/cancer.png","mini_jeu/capricorn.png","mini_jeu/gemini.png","mini_jeu/leo.png","mini_jeu/libria.png","mini_jeu/pisces.png","mini_jeu/sagittarius.png","mini_jeu/scorpio.png","mini_jeu/taurus.png"]
        self.reponse=[]
        self.solution=self.gen_solution(n)
        self.select.x=(6+6.5)*self.back_ground.scale_x-1
        self.select.y=(-6-5.5)*self.back_ground.scale_y+self.back_ground.height
        self.nb=0
        self.schedule_interval(self.affiche_sol,1.5)


    def gen_solution(self,n):
        sol=[]
        for k in range(0,n):
            if k>0:
                key2=randint(0,11)
                if key2!=key1:
                    sol.append(key2)
                key1=key2
            else:
                key1=randint(0,11)
                sol.append(key1)
        return(sol)

    def affiche_sol(self,dt):
        if self.nb==len(self.solution):
            self.unschedule(self.affiche_sol)
            self.autre()
            return 0
        affichage=Sprite(self.list_cases[self.solution[self.nb]])
        affichage.scale_y=self.back_ground.scale_y
        affichage.scale_x=self.back_ground.scale_x
        affichage.x=director.get_window_size()[0]/2
        affichage.y=director.get_window_size()[1]/2
        self.add(affichage)
        self.nb+=1


    def on_key_press(self, symbol, modifiers):
        if symbol==65293:
            self.reponse.append(self.case_courante)
            if len(self.reponse)<len(self.solution):
                if self.solution[len(self.reponse)-1]!=self.reponse[-1]:
                    director.scene.end(False)
                valide=Sprite("mini_jeu/asterisque.png")
                valide.x=director.get_window_size()[0]/2-20+(-len(self.solution)/2+len(self.reponse))*33
                valide.y=45
                self.add(valide)
            elif len(self.reponse)==len(self.solution):
                if self.solution[len(self.reponse)-1]==self.reponse[-1]:
                    self.level.ouverture()
                    self.level.coll=self.level.coll[:-1]
                    director.scene.end(True)
                else:
                    director.scene.end(False)
        elif symbol==65361:
            self.case_courante=(self.case_courante-1)%12
        elif symbol==65363:
            self.case_courante=(self.case_courante+1)%12
        elif symbol==65362:
            self.case_courante=(self.case_courante - 4)%12
        elif symbol==65364:
            self.case_courante=(self.case_courante + 4)%12

        self.select.y=(-6-5.5)*self.back_ground.scale_y+self.back_ground.height-12*(self.case_courante/4)*self.back_ground.scale_y
        self.select.x=(6+5.5)*self.back_ground.scale_x-1+(12*(self.case_courante%4))*self.back_ground.scale_x


    def autre(self):
        self.add(self.back_ground)
        self.add(self.select)

    #def on_enter(self):
