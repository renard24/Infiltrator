import os
from cocos.sprite import Sprite
from cocos.layer import Layer


class Level(Layer):
    def __init__(self,niv,n):
        self.niv=niv
        self.actuel=n
        self.coll=[]
        self.delta=60
        self.gamma=0
        self.ordi=False
        super(Level,self).__init__()
        list_img1 = os.listdir("image/")
        self.dic1 = dict()
        for k in range (0, len(list_img1),1):
            if list_img1[k] != "Thumbs.db":
                s="image/"+str(list_img1[k])
                self.dic1[str(list_img1[k]).replace('.png','')]=s
        self.gen_sol()

    def gen_sol(self):
        self.coll=[]
        if self.actuel==2:
            self.delta=32
            self.gamma=-32

        self.structure=self.build(self.niv[self.actuel-1])
        for i in range(0,len(self.structure)):
            for j in range(0,len(self.structure[i])):
                s=Sprite(self.dic1["sol1"])
                s.x=j*32+self.delta
                s.y=i*32+self.delta
                self.add(s)

        for i in range(0,len(self.structure)):
            for j in range(0,len(self.structure[i])):
                if self.structure[i][j] != "0" and self.structure[i][j] != "portev" and self.structure[i][j] != "porteh":
                    s=Sprite(self.dic1[self.structure[i][j]])
                    s.x=j*32+self.delta
                    s.y=i*32+self.delta
                    self.add(s)
                    self.coll.append([j*32+3+20+self.gamma,(i+1)*32+self.delta-3+self.gamma,(j+1)*32-3+20+self.gamma,i*32+self.delta+3+self.gamma])
        pif=0
        if self.actuel==2:
            pif=+32
        for i in range(0,len(self.structure)):
            for j in range(0,len(self.structure[i])):
                if self.structure[i][j] == "portev" or self.structure[i][j] == "porteh":
                    s=Sprite(self.dic1[self.structure[i][j]])
                    s.x=j*32+self.delta
                    s.y=i*32+self.delta
                    self.add(s)
                    self.coll.append([j*32+3+20+self.gamma,(i+1)*32+self.delta-3+32+pif,(j+1)*32-3+20+self.gamma,i*32+self.delta+3+32+pif])

    def build(self,niv):
        structure_niveau=[]
        texte=""
        #On ouvre le fichier
        with open(niv,'r') as fichier:
            for line in fichier:
                texte += line

        #On parcourt les lignes du fichier
        structure_niveau = texte.split("\n")
        for k in range(0,len(structure_niveau),1):
            structure_niveau[k] = structure_niveau[k].split()
        #On sauvegarde cette structure
        return structure_niveau

    def ouverture(self):
        for i in range(0,len(self.structure)):
            for j in range(0,len(self.structure[i])):
                if self.structure[i][j]=="portev" or self.structure[i][j]=="porteh":
                    self.structure[i][j]="sol1"
                    s=Sprite(self.dic1["sol1"])
                    s.x=j*32+self.delta
                    s.y=i*32+self.delta
                    self.add(s)
        self.draw()
