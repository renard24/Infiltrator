from cocos.scene import Scene
from cocos.layer import Layer
from cocos.sprite import Sprite
from pyglet.window import key
from cocos.director import director
from perso import *
from garde import *
from level import *
from hack import *


class Ecran_titre(Layer):
    is_event_handler=True
    def __init__(self,image):
        super(Ecran_titre,self).__init__()
        self.affichage=Sprite(image)
        self.affichage.scale=2
        self.affichage.x=self.affichage.width/2+55
        self.affichage.y=self.affichage.height/2
        self.add(self.affichage)


    def on_key_press(self,symbol,modifiers):
        if symbol==65293:
            bg_layer = ColorLayer(0,0,0,255)
            main_layer = Layer()

            fond= Level(["niveau/nivtest1.txt","niveau/niveau2.txt","niveau/niveau3.txt","niveau/niveau4.txt"],1)

            player=Perso((64,64),"image/Perso_1.png",fond)
            main_layer.add(player)

            garde=Garde("image/camegarde.png","image/vision.png", player,"Garde.txt", main_layer)
            main_layer.add(garde)





            #return Scene(bg_layer,main_layer)
            director.replace(Scene(bg_layer,fond,main_layer))
