from game_over import Game_over
from cocos.director import director
from cocos.layer import Layer,ColorLayer
from cocos.sprite import Sprite
from cocos.scene import Scene
from cocos.collision_model import CircleShape
import cocos.collision_model as cm
from cocos.rect import Rect
from garde import *
from cocos.euclid import Vector2
from pyglet.window import key
from hack import *
from cocos.scenes.pause import *
import pyglet
from level import *

SPEED_P=200

class Perso(Layer):
    is_event_handler=True
    def __init__(self,position,image,level):
        self.stop=0
        super(Perso,self).__init__()
        self.level=level
        self.x=position[0]+32
        self.y=position[1]+32
        self.sprite=Sprite(image)
        self.add(self.sprite)
        self.dt=0.1
        self.delta=32
        self.gauche = False
        self.haut = False
        self.droite = False
        self.bas = False
        self.dirx=0
        self.diry=0
        self.sprite.x=0
        self.sprite.y=0
        self.sprite.cshape=self.sprite.get_rect()
        self.sprite.cshape=(cm.AARectShape(Vector2(self.sprite.x,self.sprite.y),self.sprite.width/2,self.sprite.height/2))
        self.schedule_interval(self.move,0.01)
        # collisions="./niveau/collisions"
        #
        # self.achemin=[]
        # texte=""
        # #On ouvre le fichier
        # with open(collisions,'r') as fichier:
        #     for line in fichier:
        #         texte += line
        #
        # #On parcourt les lignes du fichier
        # self.achemin = texte.split("\n")
        # for k in range(0,len(self.achemin)-1,1):
        #     self.achemin[k] = self.achemin[k].split("&")
        #     for j in range(0,len(self.achemin[k]),1):
        #         self.achemin[k][j] = self.achemin[k][j].split()
        # for k in range(0,len(self.achemin)-1,1):
        #     for j in range(0,len(self.achemin[k]),1):
        #         self.achemin[k][j][0] = int(self.achemin[k][j][0])
        #         self.achemin[k][j][1] = int(self.achemin[k][j][1])
        #         self.achemin[k][j][2] = int(self.achemin[k][j][2])
        #         self.achemin[k][j][3] = int(self.achemin[k][j][3])
        # self.chemin=self.achemin[self.level.actuel-1]

        ordizone="./niveau/ordi.txt"

        self.aordi=[]
        texte=""
        #On ouvre le fichier
        with open(ordizone,'r') as fichier:
            for line in fichier:
                texte += line

        #On parcourt les lignes du fichier
        self.aordi = texte.split("\n")
        for k in range(0,len(self.aordi)-1,1):
            self.aordi[k] = self.aordi[k].split()
        for k in range(0,len(self.aordi)-1,1):
            self.aordi[k][0] = int(self.aordi[k][0])
            self.aordi[k][1] = int(self.aordi[k][1])
            self.aordi[k][2] = int(self.aordi[k][2])
            self.aordi[k][3] = int(self.aordi[k][3])
        self.ordi=self.aordi[self.level.actuel-1]


        portezone="./niveau/porte.txt"

        self.aporte=[]
        texte=""
        #On ouvre le fichier
        with open(portezone,'r') as fichier:
            for line in fichier:
                texte += line

        #On parcourt les lignes du fichier
        self.aporte = texte.split("\n")
        for k in range(0,len(self.aporte)-1,1):
            self.aporte[k] = self.aporte[k].split()
        for k in range(0,len(self.aporte)-1,1):
            self.aporte[k][0] = int(self.aporte[k][0])
            self.aporte[k][1] = int(self.aporte[k][1])
            self.aporte[k][2] = int(self.aporte[k][2])
            self.aporte[k][3] = int(self.aporte[k][3])
        self.porte=self.aporte[self.level.actuel-1]

    def on_key_press(self, symbol, modifiers):
        if symbol==65293 :
            if not(self.x + self.sprite.width < self.ordi[0]+self.delta +32 or self.y> self.ordi[3]+64 or self.x > self.ordi[2]+self.delta+32 or self.y+self.sprite.height < self.ordi[1]+64):
                layer_hack=Layer()
                jeu_hack=Jeu_hack("mini_jeu/fond_mini_jeu.png","mini_jeu/selection.png",self.level.actuel+3,self.level)
                layer_hack.add(jeu_hack)
                hack_porte=director.push(Scene(layer_hack))
        if symbol==65361:
            self.dirx-=1
            self.gauche=True
            self.sprite.rotation=90
        if symbol==65362:
            self.diry+=1
            self.haut=True
            self.sprite.rotation=180
        if symbol==65363:
            self.dirx+=1
            self.droite=True
            self.sprite.rotation=270
        if symbol==65364:
            self.diry-=1
            self.bas=True
            self.sprite.rotation=0
        if self.dirx<0 and self.diry>0:
            self.sprite.rotation=135
        elif self.diry>0 and self.dirx>0:
            self.sprite.rotation=225
        elif self.dirx>0 and self.diry<0:
            self.sprite.rotation=315
        elif self.diry<0 and self.dirx<0:
            self.sprite.rotation=45
        if self.stop:
            self.dirx=0
            self.diry=0
            self.stop-=1


    def on_key_release(self, symbol, modifiers):
        if symbol==65361 and self.gauche:
            self.dirx+=1
            self.gauche = False
        if symbol==65362 and self.haut:
            self.diry-=1
            self.haut = False
        if symbol==65363 and self.droite:
            self.dirx-=1
            self.droite = False
        if symbol==65364 and self.bas:
            self.diry+=1
            self.bas = False
        if self.dirx<0:
            self.sprite.rotation=90
        elif self.diry>0:
            self.sprite.rotation=180
        elif self.dirx>0:
            self.sprite.rotation=270
        elif self.diry<0:
            self.sprite.rotation=0


    def height(self):
        return(self.height)

    def width(self):
        return(self.width)

    def move(self, dt):
        self.Pos=Vector2(self.dirx,self.diry)
        self.Pos.normalize()

        n_x=self.Pos[0]*SPEED_P*dt
        n_y=self.Pos[1]*SPEED_P*dt

        self.x = self.x + n_x
        self.y = self.y + n_y
        self.sprite.x=0
        self.sprite.y=0
        self.sprite.position=(0,0)

        for chem in self.level.coll:
            if not(self.x + self.sprite.width < chem[0]+self.delta  or self.y> chem[1] or self.x > chem[2]+self.delta or self.y+self.sprite.height < chem[3]):
                self.x = self.x - n_x
                self.y = self.y - n_y


        if not(self.x + self.sprite.width < self.porte[0]+self.delta +32 or self.y> self.porte[3]+64 or self.x > self.porte[2]+self.delta+32 or self.y+self.sprite.height < self.porte[1]+64):
            if self.level.actuel==2:
                director.replace(Scene(Game_over("fonds/ecran_titre.png")))
            self.ordi=self.aordi[self.level.actuel]
            self.porte=self.aporte[self.level.actuel]
            bg_layer = ColorLayer(0,0,0,255)
            main_layer = Layer()

            fond= Level(["niveau/nivtest1.txt","niveau/niveau2.txt","niveau/niveau3.txt","niveau/niveau4.txt"],self.level.actuel+1)


            self.x=64+32*3
            self.y=64+32*2-16
            self.sprite.x=0
            self.sprite.y=0
            self.sprite.position=(0,0)
            self.gauche = False
            self.droite = False
            self.haut = False
            self.bas = False
            self.dirx=0
            self.diry=0
            self.stop=2

            self.level=fond
            #player=Perso((64,64),"image/Perso_1.png",fond)
            main_layer.add(self)

            garde=Garde("image/camegarde.png","image/vision.png", self,"Garde.txt", main_layer)
            main_layer.add(garde)


            director.replace(Scene(bg_layer,fond,main_layer))




        #self.sprite.cshape=(cm.AARectShape(Vector2(self.sprite.x,self.sprite.y),self.sprite.width/2,self.sprite.height/2))

    #def on_exit(self):
